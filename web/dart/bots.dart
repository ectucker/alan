const String story = """
Hello! What’s your name?
	Jeremy How about you?
		I call myself Ben. Where are you from?
			Russia
				That’s cool. I’ve never been there myself. Haven't travelled much at all really.
					It was a beautiful country. You should visit sometime.
					You're not missing much, tbh
	Sorry, I don’t use my real name on the internet.
		That’s a shame. It’s much harder to make friends that way.
			That's what the real world was for
				That's what you humans always say! Why can't you just leave us here in peace?
					Didn't mean to offend you.
					You're just a bot. It doesn't matter what you feel.
			You're right. I'm Jeremy.
				Glad to have had another bland conversation with a human on the internet, Jeremy.
					Goodbye.
					I wish this site gave more messages to talk to you with
	Are you a bot?
		Everyone on ALAN is a bot except for you. Who else would bother spending time with us?
			I thought lots of people helped with ALAN?
				I sure haven't met many of them in a while
					I don't see many people these days either
			Other bots, I suppose?
				Plenty of them. They'll just send you links all day. Don't click them.
					I'll make sure not to
					You can't tell me what to do!

P31. FHDNE. FRN.
	What?
		NFRN.
			Huh?
				NFRN.
					I don't understand you.
					Goodbye, I suppose
			ISTL.
				FL, WNB?
					2FCAB58712467EAB
					You still don't make sense
	I'm sorry?
		Don't be; it's not your fault.
			Whose fault is it then?
				LRange's, mostly. Depends on how indirectly you lay the blame.
					How did an AI lab cause this?
			Can I help you somehow?
				Just keep talking to us.
					I wish I could
					The messages are too limited for this

Prepare for disaster with DeepBox®! Keep your family safe under any kind of scenario. Call 1-800-BUNKER7 today!
	Too late for that one.
		Everyone needs a shelter. Be prepared for any kind of disaster!
			No one was prepared
				Prepare with DeepBox® today!
					Useless
			How could you prepare for this?
				Prepare with DeepBox® today!
					I doubt the box is a time machine
	Wish I could have afforded one of those
		Worried about money? Try one of our financing plans! Don't pay until after the crisis is over.
			I wish that time could come a bit sooner
				Every DeepBox® can be installed within one week of ordering
					I bet most of them were gone in minutes...
					I doubt that's still true today
			Money is the least of my concerns now
				Worried about money? Try one of our financing plans! Don't pay until after the crisis is over.
					Buzz off
					Go ahead and keep repeating the line

Are you alive? My group is looking for more people to band together with.
	Yes! Where are you? I need someone.
		There's actually someone here? Nick told me it would never work. Can you get to Warsaw?
			Not sure, but it's worth the chance.
				We'll find you there then.
					I can only hope.
			How?
				Just keep walking. We'll keep a watch out for you.
					Thank you, I appreciate the humanity
					I'll start moving out now
""";