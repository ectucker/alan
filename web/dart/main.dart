import 'dart:html';
import 'bots.dart';
import 'parser.dart';

List<Node> convos = parseDocument(story);
Node current = convos.removeAt(0);

void main() {
  querySelectorAll('.eval').forEach((Element elem) {
    elem.onClick.listen((mouse) {
      if (convos.length > 0) {
        current = convos.removeAt(0);
        querySelector('#chat').children.clear();
        querySelector('#evaluation').hidden = true;
        Future.delayed(Duration(milliseconds: 700))
            .then((onValue) => advanceNode(current));
      } else {
        querySelector('#chat').children.clear();
        querySelector('#evaluation').hidden = true;
        Future.delayed(Duration(milliseconds: 500))
            .then((onValue) => querySelector('#end').hidden = false);
      }
    });
  });
  querySelector('#start').onClick.listen((mouse) {
    querySelector('#intro').hidden = true;
    querySelector('#chat').hidden = false;
    Future.delayed(Duration(milliseconds: 700))
        .then((onValue) => advanceNode(current));
  });
}

void addMessage(String text){
  var message = ParagraphElement();
  message.text = text;
  message.classes.add("message");
  querySelector('#chat').children.add(message);
}

void advanceNode(Node node) {
  addMessage(node.text);
  querySelector('#outeroptions').hidden = false;
  querySelector('#options').children.clear();
  for (Node choice in node.children) {
    var option = AnchorElement(href: '#');
    option.text = choice.text;
    option.onClick.first.then((mouse) {
      querySelector('#options').children.clear();
      addMessage(choice.text);
      if (choice.children.isNotEmpty) {
        Future.delayed(Duration(milliseconds: 1000))
            .then((onValue) => advanceNode(choice.children.first));
      } else {
        querySelector('#evaluation').hidden = false;
        querySelector('#outeroptions').hidden = true;
      }
    });
    option.classes.add("option");
    querySelector('#options').children.add(option);
  }
  current = node;
}
