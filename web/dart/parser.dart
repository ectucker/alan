import 'dart:convert';

class Node {

  String text;
  List<Node> children;

  Node(this.text){
    children = new List<Node>();
  }

  @override
  String toString() {
    String str = text;
    for (Node node in children) {
      str += "\n\t";
      str += node.toString();
    }
    return str;
  }
}

List<Node> parseDocument(String doc) {
  List<Node> nodes = new List<Node>();

  Map<int, Node> prev = new Map<int, Node>();

  LineSplitter ls = new LineSplitter();
  List<String> lines = ls.convert(doc);
  for (String line in lines) {
    int indents = 0;
    while (line.contains('\t')) {
       line = line.replaceFirst('\t', '');
       indents++;
    }
    Node node = new Node(line);
    //print(indents);
    //print(line);
    if (line.isNotEmpty) {
      if (indents == 0) {
        nodes.add(node);
        prev.clear();
        prev[0] = node;
      } else {
        //print("Child of: ${prev[indents - 1].text}");
        prev[indents - 1].children.add(node);
        prev[indents] = node;
      }
    }
  }

  return nodes;
}
